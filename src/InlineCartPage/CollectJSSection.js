import React from 'react';

export default class CollectJSSection extends React.Component {
  // Created the 3 divs for the CollectJS iframes
  state = {
    toggle: 0
  }

  clickHandler = () => {
    this.setState({
      toggle: this.state.toggle ^ 1
    })
  }
  render() {
    return (
      <React.Fragment>
        <button onClick={this.clickHandler}> Click </button> <br/>
        {this.state.toggle}
        
        {
          this.state.toggle  === 1 && (
            <>
              <div id="ccnumber" />
              <div id="ccexp" />
              <div id="cvv" />
            </>
          ) 
        }
        { this.state.toggle  === 0 && (
            <>
              <div id="check_account_number" />
              <div id="check_routing_number" />
              <div id="check_account_name"/>
            </>
          )
        }

      </React.Fragment>
    );
  }
}
